import os
from libs import trading_lib as tl
import pandas as pd
import numpy as np
import datetime
from datetime import datetime
from dateutil.relativedelta import relativedelta
import pprint as pp

# Global variables
capital = 10_000
newest_dates = [datetime(2007, 1, 1)]  # Стартовая дата (можно не указывать)
oldest_dates = [datetime.now()]  # Конечная дата (можно не указывать)

# Чеки
bool_reload_data = 0
bool_sma = 0 if bool_reload_data == 0 else 1
bool_mom = 0 if bool_reload_data == 0 else 1
bool_rebalance = 1  # 1 в конце месяца, 0 в начале месяца
chart_or_save = 1  # 1 вывести итоговый график, -1 сохранить файл, 0 вывести график и сохранить файл

# Условные константы
BASE_DIR = 'historical_data'
SIGNAL_EQUITY = 'DIA'
SIGNAL_BONDS = 'TLT'
SMA_PERIOD = 200
SMA_UP = 1.000
SMA_DOWN = 1.000
MOM_EQUITY_PERIOD = 2  # Окно моментума. Колонку посчитаем сразу как для последнего числа месяца, так и для первого
MOM_BONDS_PERIOD = 2

REBALANCE_LIMIT = 0.05  # Допустимое отклонение для активов без ребаланса
DIV_TAX = 0.9  # Что остаётся после налога на дивы
IB_COMM = 0.0055

high_risk = {
    'QLD': 0.25,
    'FBT': 0.25,
    'FDN': 0.25,
    'DDM': 0.25
}

mid_risk = {
    'QLD': 0.15,
    'IVW': 0.35,
    'FBT': 0.2,
    'FDN': 0.2,
    'TLT': 0.1
}

high_safe = {
    'TLT': 0.34,
    'IEF': 0.33,
    'FXY': 0.33
}

mid_safe = {
    'XLP': 0.2,
    'TLT': 0.2,
    'IEF': 0.2,
    'FXY': 0.2
}


def calculate_sma(df: pd.DataFrame, ticker: str):
    df['SMA_' + str(SMA_PERIOD)] = round(df['Close'].rolling(SMA_PERIOD).mean(), 2)
    tl.save_csv(BASE_DIR, ticker, df)


def calculate_momentum(df: pd.DataFrame, ticker: str):
    df.Date = pd.to_datetime(df.Date)

    temp_list = []
    for i in range(len(df.Date)):
        if i != len(df)-1 and df.Date[i].month != df.Date[i + 1].month:
            month_close_finder(df, i, 'end_month', temp_list)
        elif i != 0 and df.Date[i].month != df.Date[i - 1].month:
            month_close_finder(df, i, 'start_month', temp_list)
        else:
            temp_list.append(None)

    df['Momentum_' + str(MOM_EQUITY_PERIOD)] = temp_list
    tl.save_csv(BASE_DIR, ticker, df)


# Находим закрытие N-месяцев назад
def month_close_finder(df: pd.DataFrame, cur_date: int, rebalance_type: str, temp_list: list):
    find_data = df.Date[cur_date] - relativedelta(months=MOM_EQUITY_PERIOD)
    column_date = df[(df.Date.dt.year == find_data.year) & (df.Date.dt.month == find_data.month)].Close

    if len(column_date) != 0 and rebalance_type == 'end_month':
        temp_list.append(round(df.Close[cur_date] / column_date.iloc[-1], 3))
    elif len(column_date) != 0 and rebalance_type == 'start_month':
        temp_list.append(round(df.Close[cur_date] / column_date.iloc[0], 3))
    else:
        temp_list.append(None)


# Создаём словари с данными по каждому портфелю и ищем старейшую и новейшую дату
def dict_tickers_date(dict_: dict) -> dict:
    temp_dict = {}
    for t in dict_.keys():
        temp_dict[str(t)] = tl.load_csv(str(t))
        newest_dates.append(temp_dict[t]['Date'][0])
        oldest_dates.append(temp_dict[t]['Date'].iloc[-1])
    return temp_dict


# Обрезаем данные по старейшей и новейшей дате
def cut_data(dict_: dict, start: datetime, end: datetime) -> dict:
    temp_dict = {}
    for t in dict_.keys():
        temp_dict[str(t)] = dict_[t].loc[
            (dict_[t]['Date'] >= start) & (dict_[t]['Date'] <= end)].reset_index(drop=True)
    return temp_dict


# ---------------------------------------------------------------------------------------------------------------------
def dont_have_any_port(port_weight: dict, port_data: dict, table_number: int):
    tickers = list(port_weight.keys())
    total_cap_after_trades = 0
    total_pos_weight = 0

    for t in tickers:
        capital_after_comm = port_weight[t] * capital - \
                             port_weight[t] * capital / port_data[t]['Close'][table_number] * IB_COMM
        strategy_dict['Ticker_' + str(tickers.index(t) + 1)][table_number] = t
        strategy_dict['Shares_' + str(tickers.index(t) + 1)][table_number] = capital_after_comm / port_data[t]['Close'][
            table_number]
        strategy_dict['DivTicker_' + str(tickers.index(t) + 1)][table_number] = 0
        strategy_dict['Price_' + str(tickers.index(t) + 1)][table_number] = port_data[t]['Close'][table_number]
        total_cap_after_trades += capital_after_comm
        total_pos_weight += port_weight[t]

    strategy_dict['Cash'][table_number] = (1 - total_pos_weight) * total_cap_after_trades
    strategy_dict['Capital'][table_number] = total_cap_after_trades


def changing_port(port_weight: dict, port_data: dict, table_number: int):
    global past_port, past_data
    max_positions_in_port = (
        max(len(high_risk.keys()), len(mid_risk.keys()), len(high_safe.keys()), len(mid_safe.keys())))
    
    # Учитываем дивиденды и считаем общий капитал порта на закрытие/открытие дня
    old_positions = len(past_port.keys())
    total_cap = strategy_dict['Cash'][table_number - 1]
    for i in range(1, old_positions + 1):
        total_cap += strategy_dict['Shares_' + str(i)][table_number - 1] * \
                     past_data[strategy_dict['Ticker_' + str(i)][table_number - 1]]['Close'][table_number]
        div_storage.append(strategy_dict['Shares_' + str(i)][table_number - 1] *
                           past_data[strategy_dict['Ticker_' + str(i)][table_number - 1]]['Dividend'][table_number] *
                           DIV_TAX)
    total_cap += sum(div_storage)

    commissions = 0
    # Считаем комиссии по тикерам, которые останутся в новом портфеле и которые полностью сбрасываем
    for i in range(1, max_positions_in_port + 1):
        if strategy_dict['Ticker_' + str(i)][table_number - 1] in (port_weight.keys()) and \
                strategy_dict['Ticker_' + str(i)][table_number - 1] != 0:

            shares_for_cur_ticker = total_cap * port_weight[strategy_dict['Ticker_' + str(i)][table_number - 1]] / \
                                    past_data[strategy_dict['Ticker_' + str(i)][table_number - 1]]['Close'][
                                        table_number]
            shares_for_trade = shares_for_cur_ticker - strategy_dict['Shares_' + str(i)][table_number - 1]
            commissions += commissions_calc(abs(shares_for_trade))

        elif strategy_dict['Ticker_' + str(i)][table_number - 1] not in (port_weight.keys()) and \
                strategy_dict['Ticker_' + str(i)][table_number - 1] != 0:

            commissions += commissions_calc(abs(strategy_dict['Shares_' + str(i)][table_number - 1]))

    # Считаем комиссии по тикерам, которых не было в прошлом порте
    new_tickers_list = list(port_weight.keys() - past_port.keys())
    for ticker in new_tickers_list:
        cur_ticker_shares = port_weight[ticker] * total_cap / port_data[ticker]['Close'][table_number]
        commissions += commissions_calc(abs(cur_ticker_shares))

    total_cap -= commissions
    # Заполняем strategy_dict с новым портфелем
    tickers = list(port_weight.keys())
    total_pos_weight = 0
    for t in tickers:
        strategy_dict['Ticker_' + str(tickers.index(t) + 1)][table_number] = t
        strategy_dict['Shares_' + str(tickers.index(t) + 1)][table_number] = port_weight[t] * total_cap / \
                                                                             port_data[t]['Close'][table_number]
        strategy_dict['DivTicker_' + str(tickers.index(t) + 1)][table_number] = 0
        strategy_dict['Price_' + str(tickers.index(t) + 1)][table_number] = port_data[t]['Close'][table_number]
        total_pos_weight += port_weight[t]

    strategy_dict['Cash'][table_number] = (1 - total_pos_weight) * total_cap
    strategy_dict['Capital'][table_number] = total_cap


def every_day_check(table_number: int):
    global past_port, past_data

    position = len(past_port.keys())
    total_cap_after_day = 0
    for pos_num in range(1, position + 1):
        strategy_dict['Ticker_' + str(pos_num)][table_number] = strategy_dict['Ticker_' + str(pos_num)][
                                                            table_number - 1]
        strategy_dict['Shares_' + str(pos_num)][table_number] = strategy_dict['Shares_' + str(pos_num)][
                                                            table_number - 1]
        strategy_dict['DivTicker_' + str(pos_num)][table_number] = past_data[strategy_dict['Ticker_' +
                                                                str(pos_num)][table_number]]['Dividend'][table_number]
        strategy_dict['Price_' + str(pos_num)][table_number] = past_data[strategy_dict['Ticker_' + str(pos_num)]
                                                            [table_number]]['Close'][table_number]
        total_cap_after_day += strategy_dict['Shares_' + str(pos_num)][table_number] * \
                            past_data[strategy_dict['Ticker_' + str(pos_num)][table_number]]['Close'][table_number]
        div_storage.append(
            strategy_dict['Shares_' + str(pos_num)][table_number] * strategy_dict['DivTicker_' + str(pos_num)][
                table_number] * DIV_TAX)

    strategy_dict['Cash'][table_number] = strategy_dict['Cash'][table_number - 1]
    strategy_dict['Capital'][table_number] = total_cap_after_day + strategy_dict['Cash'][table_number]


def commissions_calc(shares: float) -> float:
    if shares >= 100.0:
        return shares * IB_COMM
    else:
        return IB_COMM * 100


def rebalance_trades(table_number: int):
    # Словари с данными
    global high_risk_data, mid_risk_data, high_safe_data, mid_safe_data, signal_data, strategy_dict
    global past_port, past_data, div_storage

    # No_port and high_risk
    if past_port == '' \
            and signal_data[SIGNAL_EQUITY]['Close'][table_number] > signal_data[SIGNAL_EQUITY]['SMA_'+str(SMA_PERIOD)][table_number] * SMA_UP \
            and signal_data[SIGNAL_EQUITY]['Momentum_' + str(MOM_EQUITY_PERIOD)][table_number] >= 1 \
            and signal_data[SIGNAL_BONDS]['Momentum_' + str(MOM_BONDS_PERIOD)][table_number] < 1:

        dont_have_any_port(high_risk, high_risk_data, table_number)
        past_port = high_risk
        past_data = high_risk_data

    # No_port and mid_risk
    elif past_port == '' \
            and signal_data[SIGNAL_EQUITY]['Close'][table_number] > signal_data[SIGNAL_EQUITY]['SMA_' + str(SMA_PERIOD)][table_number] * SMA_UP \
            and (signal_data[SIGNAL_EQUITY]['Momentum_' + str(MOM_EQUITY_PERIOD)][table_number] < 1
                 or signal_data[SIGNAL_BONDS]['Momentum_' + str(MOM_BONDS_PERIOD)][table_number] >= 1):

        dont_have_any_port(mid_risk, mid_risk_data, table_number)
        past_port = mid_risk
        past_data = mid_risk_data

    # No_port and high_safe
    elif past_port == '' \
            and signal_data[SIGNAL_EQUITY]['Close'][table_number] < signal_data[SIGNAL_EQUITY]['SMA_' + str(SMA_PERIOD)][table_number] * SMA_DOWN \
            and signal_data[SIGNAL_EQUITY]['Momentum_' + str(MOM_EQUITY_PERIOD)][table_number] < 1:

        dont_have_any_port(high_safe, high_safe_data, table_number)
        past_port = high_safe
        past_data = high_safe_data

    # No_port and mid_safe
    elif past_port == '' \
            and signal_data[SIGNAL_EQUITY]['Close'][table_number] < signal_data[SIGNAL_EQUITY]['SMA_' + str(SMA_PERIOD)][table_number] * SMA_DOWN \
            and (signal_data[SIGNAL_EQUITY]['Momentum_' + str(MOM_EQUITY_PERIOD)][table_number] >= 1
                 or signal_data[SIGNAL_BONDS]['Momentum_' + str(MOM_BONDS_PERIOD)][table_number] >= 1):

        dont_have_any_port(mid_safe, mid_safe_data, table_number)
        past_port = mid_safe
        past_data = mid_safe_data

    # High_risk
    elif signal_data[SIGNAL_EQUITY]['Close'][table_number] > signal_data[SIGNAL_EQUITY]['SMA_'+str(SMA_PERIOD)][table_number] * SMA_UP \
            and signal_data[SIGNAL_EQUITY]['Momentum_' + str(MOM_EQUITY_PERIOD)][table_number] >= 1 \
            and signal_data[SIGNAL_BONDS]['Momentum_' + str(MOM_BONDS_PERIOD)][table_number] < 1:

        if past_port != high_risk:
            changing_port(high_risk, high_risk_data, table_number)
            div_storage = []
        else:
            every_day_check(table_number)

        past_port = high_risk
        past_data = high_risk_data

    # Mid_risk
    elif signal_data[SIGNAL_EQUITY]['Close'][table_number] > signal_data[SIGNAL_EQUITY]['SMA_' + str(SMA_PERIOD)][table_number] * SMA_UP \
            and (signal_data[SIGNAL_EQUITY]['Momentum_' + str(MOM_EQUITY_PERIOD)][table_number] < 1
                 or signal_data[SIGNAL_BONDS]['Momentum_' + str(MOM_BONDS_PERIOD)][table_number] >= 1):

        if past_port != mid_risk:
            changing_port(mid_risk, mid_risk_data, table_number)
            div_storage = []
        else:
            every_day_check(table_number)

        past_port = mid_risk
        past_data = mid_risk_data

    # High_safe
    elif signal_data[SIGNAL_EQUITY]['Close'][table_number] < signal_data[SIGNAL_EQUITY]['SMA_' + str(SMA_PERIOD)][table_number] * SMA_DOWN \
            and signal_data[SIGNAL_EQUITY]['Momentum_' + str(MOM_EQUITY_PERIOD)][table_number] < 1:

        if past_port != high_safe:
            changing_port(high_safe, high_safe_data, table_number)
            div_storage = []
        else:
            every_day_check(table_number)

        past_port = high_safe
        past_data = high_safe_data

    # Mid_safe
    elif signal_data[SIGNAL_EQUITY]['Close'][table_number] < signal_data[SIGNAL_EQUITY]['SMA_' + str(SMA_PERIOD)][table_number] * SMA_DOWN \
            and signal_data[SIGNAL_EQUITY]['Momentum_' + str(MOM_EQUITY_PERIOD)][table_number] >= 1 \
            or signal_data[SIGNAL_BONDS]['Momentum_' + str(MOM_BONDS_PERIOD)][table_number] >= 1:

        if past_port != mid_safe:
            changing_port(mid_safe, mid_safe_data, table_number)
            div_storage = []
        else:
            every_day_check(table_number)

        past_port = mid_safe
        past_data = mid_safe_data

    else:
        print(f"Макс, мы балансируемся вне условий на дату {signal_data[SIGNAL_EQUITY]['Date'][i]}!")
        print()
        print('(Eq-SMA) / SMA ' + str(
            (signal_data[SIGNAL_EQUITY]['Close'][i] - signal_data[SIGNAL_EQUITY]['SMA_' + str(SMA_PERIOD)][i]) /
            signal_data[SIGNAL_EQUITY]['SMA_' + str(SMA_PERIOD)][i]))
        print(f"Eq_Mom {signal_data[SIGNAL_EQUITY]['Momentum_' + str(MOM_EQUITY_PERIOD)][i]}")
        print(f"Bond_Mom {signal_data[SIGNAL_BONDS]['Momentum_' + str(MOM_BONDS_PERIOD)][i]}")
        print('----------------------------------------------------------------')

# ----------------------------------------------------------------------------------------------------------------------


if __name__ == '__main__':
    # Сложить ключи всех словарей, вывести уникальные и проверить если ли по ним data, и чекнуть на требование скачки
    all_tickers = {**high_risk, **mid_risk, **high_safe, **mid_safe}
    tickers_list = set([key for key in all_tickers.keys()] + [SIGNAL_EQUITY, SIGNAL_BONDS])

    for t in tickers_list:
        if os.path.isfile(os.path.join(BASE_DIR, str(t) + '.csv')) is False or bool_reload_data:
            tl.download_yahoo(t)

    # Рассчитать SMA для DIA и сохранить колонку в DIA файл. И есть ли чек на расчёт SMA.
    if bool_sma:
        calculate_sma(pd.read_csv(BASE_DIR + '/' + SIGNAL_EQUITY + '.csv'), SIGNAL_EQUITY)

    # Рассчитать моментумы RISKON_TICKER и RISKOFF_TICKER, сохранить колонку в файлы. Есть ли чек на расчёт моментума.
    if bool_mom:
        calculate_momentum(pd.read_csv(BASE_DIR + '/' + SIGNAL_EQUITY + '.csv'), SIGNAL_EQUITY)
        calculate_momentum(pd.read_csv(BASE_DIR + '/' + SIGNAL_BONDS + '.csv'), SIGNAL_BONDS)

    # Создаём словари с датой, так как количество тикеров - неизвестно
    high_risk_data = dict_tickers_date(high_risk)
    mid_risk_data = dict_tickers_date(mid_risk)
    high_safe_data = dict_tickers_date(high_safe)
    mid_safe_data = dict_tickers_date(mid_safe)
    signal_data = dict_tickers_date({SIGNAL_EQUITY: '', SIGNAL_BONDS: ''})

    # Обрезаем дату тикеров так, чтобы была одинаковая размерность
    start, end = max(newest_dates), min(oldest_dates)

    high_risk_data = cut_data(high_risk_data, start, end)
    mid_risk_data = cut_data(mid_risk_data, start, end)
    high_safe_data = cut_data(high_safe_data, start, end)
    mid_safe_data = cut_data(mid_safe_data, start, end)
    signal_data = cut_data(signal_data, start, end)

    # Создаём словарь с динамикой капитала и колонками по портфелю в моменте
    max_positions_in_port = (
        max(len(high_risk.keys()), len(mid_risk.keys()), len(high_safe.keys()), len(mid_safe.keys())))
    first_ticker = list(high_risk_data.keys())[0]

    strategy_dict = {}
    strategy_dict['Date'] = high_risk_data[first_ticker]['Date']
    for i in range(1, max_positions_in_port+1):
        strategy_dict['Ticker_'+str(i)] = [0] * len(high_risk_data[first_ticker])
        strategy_dict['Shares_' + str(i)] = [0] * len(high_risk_data[first_ticker])
        strategy_dict['DivTicker_' + str(i)] = [0] * len(high_risk_data[first_ticker])
        strategy_dict['Price_' + str(i)] = [0] * len(high_risk_data[first_ticker])
    strategy_dict['Cash'] = [0] * len(high_risk_data[first_ticker])
    strategy_dict['Capital'] = [0] * len(high_risk_data[first_ticker])

    # Берём из любой data торговые даты и начинаем перебирать
    past_port = ''
    past_data = ''
    div_storage = []
    dates_list = high_risk_data[first_ticker]['Date']

    for table_number in range(len(dates_list)):
        if table_number != len(dates_list)-1 and bool_rebalance and dates_list[table_number].month != \
                dates_list[table_number+1].month:
            rebalance_trades(table_number)

        elif table_number != 0 and bool_rebalance == 0 and dates_list[table_number].month != \
                dates_list[table_number-1].month:
            rebalance_trades(table_number)

        elif past_port != '':
            every_day_check(table_number)
            
        # В дальнешем здесь будет также находится внутримесячная проверка на VIX/VIX3M, если она срабатывает: ...

    df_strategy = pd.DataFrame.from_dict(strategy_dict)
    df_strategy = df_strategy[df_strategy.Capital != 0]
    df_strategy['Capital'] = df_strategy.Capital.astype(int)

    if chart_or_save == 1:
        tl.plot_capital(list(df_strategy.Date), list(df_strategy.Capital))
    elif chart_or_save == -1:
        tl.save_csv(BASE_DIR,
                    f"InvestModel_EQ'{SIGNAL_EQUITY}_BN'{SIGNAL_BONDS}_SMA'{SMA_PERIOD}_MomEQ'{MOM_EQUITY_PERIOD}_MomBN'{MOM_BONDS_PERIOD}_Close'{bool_rebalance}",
                    df_strategy)
    elif chart_or_save == 0:
        tl.plot_capital(list(df_strategy.Date), list(df_strategy.Capital))
        tl.save_csv(BASE_DIR,
                    f"InvestModel_EQ'{SIGNAL_EQUITY}_BN'{SIGNAL_BONDS}_SMA'{SMA_PERIOD}_MomEQ'{MOM_EQUITY_PERIOD}_MomBN'{MOM_BONDS_PERIOD}_Close'{bool_rebalance}",
                    df_strategy)
    else:
        print('Передано неверное значение chart_or_save')
